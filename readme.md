# Criteria Query Error

Based on https://github.com/arnosthavelka/spring-advanced-training/tree/dzone-querydsl-sb3.0.2

Simplified to provide a test case for https://hibernate.atlassian.net/jira/software/c/projects/HHH/issues/HHH-17135

Test case to pass nullLiteral in CriteriaBuilder.construct 