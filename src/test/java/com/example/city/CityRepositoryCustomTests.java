package com.example.city;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

/**
 * See https://www.baeldung.com/rest-api-search-language-spring-data-querydsl
 */
@DataJpaTest
class CityRepositoryCustomTests {

	@Autowired
	protected CityRepository cityRepository;

	@Test
	void searchByCity() {
		var cityName = "London";

		var result = cityRepository.searchByCity(cityName);

		assertThat(result)
				.hasSize(1)
				.first()
				.satisfies(c -> {
					assertThat(c.getId()).isPositive();
					assertThat(c.getName()).isEqualTo(cityName);
					assertThat(c.getState()).isNull();
					assertThat(c.getCountryName()).isEqualTo(null);
				});
	}
}
