package com.example.city;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.query.QueryByExampleExecutor;

public interface CityRepository extends CityCustomRepository,
		JpaRepository<City, Long>, JpaSpecificationExecutor<City>, QueryByExampleExecutor<City> {
}
