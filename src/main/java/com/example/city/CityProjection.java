package com.example.city;

import com.example.country.Country;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CityProjection {

    private Long id;
    private String name;
    private String state;
    private String countryName;

    public CityProjection(Long id, String name, String state, Country country) {
        this.id = id;
        this.name = name;
        this.state = state;
        if (country != null) {
            this.countryName = country.getName();
        }
    }

}
