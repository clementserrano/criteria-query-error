package com.example.city;

import lombok.NonNull;

import java.util.List;

public interface CityCustomRepository {

	List<CityProjection> searchByCity(@NonNull String cityName);

}
