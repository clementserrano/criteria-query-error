package com.example.city;

import com.example.country.Country;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.criteria.Predicate;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

import static com.example.city.City_.*;

@Repository
@RequiredArgsConstructor
public class CityCustomRepositoryImpl implements CityCustomRepository {

	@PersistenceContext
	private final EntityManager em;

	public List<CityProjection> searchByCity(@NonNull String cityName) {
		var cb = em.getCriteriaBuilder();
		var query = cb.createQuery(CityProjection.class);
		var cityRoot = query.from(City.class);

		List<Predicate> predicates = new ArrayList<>();
		predicates.add(cb.like(cityRoot.get(name), cityName));

		query.select(
				cb.construct(CityProjection.class,
						cityRoot.get(id),
						cityRoot.get(name),
						cityRoot.get(state),
                        cb.nullLiteral(Country.class)))
				.where(predicates.toArray(new Predicate[0]));
		return em.createQuery(query).getResultList();
	}
}
